# spring reconciliation system

A spring boot reconciliation app that parses CSV and JSON transaction files, compare them, and returns the matched, mismatched, and missing transactions.

## Getting started

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ps.adam.shannag/reconciliation-system.git
git branch -M main
git push -uf origin main
```

