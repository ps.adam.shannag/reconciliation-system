package com.ps.intern.spring.reconciliation.system.services.Session;

import com.ps.intern.spring.reconciliation.system.dao.SessionRepository;
import com.ps.intern.spring.reconciliation.system.dao.UserRepository;
import com.ps.intern.spring.reconciliation.system.models.session.Session;
import com.ps.intern.spring.reconciliation.system.models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SessionServiceImpl implements SessionService{

    private SessionRepository sessionRepository;
    private UserRepository userRepository;

    @Autowired
    public SessionServiceImpl(SessionRepository sessionRepository, UserRepository userRepository) {
        this.sessionRepository = sessionRepository;
        this.userRepository = userRepository;
    }

    @Override
    public boolean save(Session session, String user_name) {
        Optional<User> user = userRepository.findByUserName(user_name);
        user.get().addSession(session);
        sessionRepository.save(session);
        return true;
    }
}
