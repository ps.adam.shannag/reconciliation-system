package com.ps.intern.spring.reconciliation.system.dao;

import com.ps.intern.spring.reconciliation.system.models.session.Session;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Integer> {
}
