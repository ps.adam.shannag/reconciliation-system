package com.ps.intern.spring.reconciliation.system.models.session;

public class SessionRequest {
    private String sfn,tfn;
    private int matched,mismatched,missing;

    public SessionRequest(String sfn, String tfn, int matched, int mismatched, int missing) {
        this.sfn = sfn;
        this.tfn = tfn;
        this.matched = matched;
        this.mismatched = mismatched;
        this.missing = missing;
    }

    public String getSfn() {
        return sfn;
    }

    public void setSfn(String sfn) {
        this.sfn = sfn;
    }

    public String getTfn() {
        return tfn;
    }

    public void setTfn(String tfn) {
        this.tfn = tfn;
    }

    public int getMatched() {
        return matched;
    }

    public void setMatched(int matched) {
        this.matched = matched;
    }

    public int getMismatched() {
        return mismatched;
    }

    public void setMismatched(int mismatched) {
        this.mismatched = mismatched;
    }

    public int getMissing() {
        return missing;
    }

    public void setMissing(int missing) {
        this.missing = missing;
    }
}
