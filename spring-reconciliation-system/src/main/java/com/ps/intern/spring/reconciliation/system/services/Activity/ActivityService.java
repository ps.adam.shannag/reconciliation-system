package com.ps.intern.spring.reconciliation.system.services.Activity;

import com.ps.intern.spring.reconciliation.system.models.activity.Activity;
import com.ps.intern.spring.reconciliation.system.models.session.Session;


public interface ActivityService {
    void save(Activity activity, Session session);
}
