package com.ps.intern.spring.reconciliation.system.services.User;

import com.ps.intern.spring.reconciliation.system.models.user.User;


public interface UserService {
   User findByUserName(String userName);
}
