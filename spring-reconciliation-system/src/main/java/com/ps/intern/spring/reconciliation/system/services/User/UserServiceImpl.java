package com.ps.intern.spring.reconciliation.system.services.User;

import com.ps.intern.spring.reconciliation.system.dao.UserRepository;
import com.ps.intern.spring.reconciliation.system.models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findByUserName(String userName) {
        return this.userRepository.findByUserName(userName).get();
    }
}
