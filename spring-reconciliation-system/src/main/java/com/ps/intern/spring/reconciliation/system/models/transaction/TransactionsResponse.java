package com.ps.intern.spring.reconciliation.system.models.transaction;

import com.intern.reconciliation.system.transaction.Transaction;

import java.util.List;

public class TransactionsResponse {
    // TODO, make sure that the variables has private accessor
    List<Transaction> matching,mismatching,missing;
    String fileCode;

    public TransactionsResponse(List<Transaction> matching, List<Transaction> mismatching, List<Transaction> missing, String fileCode) {
        this.matching = matching;
        this.mismatching = mismatching;
        this.missing = missing;
        this.fileCode = fileCode;
    }

    public List<Transaction> getMatching() {
        return matching;
    }

    public void setMatching(List<Transaction> matching) {
        this.matching = matching;
    }

    public List<Transaction> getMismatching() {
        return mismatching;
    }

    public void setMismatching(List<Transaction> mismatching) {
        this.mismatching = mismatching;
    }

    public List<Transaction> getMissing() {
        return missing;
    }

    public void setMissing(List<Transaction> missing) {
        this.missing = missing;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }
}
