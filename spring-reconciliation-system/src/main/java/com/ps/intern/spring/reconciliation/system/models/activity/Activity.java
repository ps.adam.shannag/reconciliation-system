package com.ps.intern.spring.reconciliation.system.models.activity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "`activity`")
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name ="`time`")
    private Date time;

    @Column(name ="`source`")
    private String source;

    @Column(name="`target`")
    private String target;

    @Column(name="`matched`")
    private int matched;

    @Column(name="`mismatched`")
    private int mismatched;

    @Column(name="`missing`")
    private int missing;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public int getMatched() {
        return matched;
    }

    public void setMatched(int matched) {
        this.matched = matched;
    }

    public int getMismatched() {
        return mismatched;
    }

    public void setMismatched(int mismatched) {
        this.mismatched = mismatched;
    }

    public int getMissing() {
        return missing;
    }

    public void setMissing(int missing) {
        this.missing = missing;
    }
}
