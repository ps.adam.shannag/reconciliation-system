package com.ps.intern.spring.reconciliation.system.util;

import com.intern.reconciliation.system.parser.CSVTransactionParser;
import com.intern.reconciliation.system.parser.JSONTransactionParser;
import com.intern.reconciliation.system.parser.ParserService;
import com.intern.reconciliation.system.transaction.Transaction;
import com.intern.reconciliation.system.transaction.TransactionManager;
import com.intern.reconciliation.system.transaction.TransactionManagerService;

import java.util.ArrayList;
import java.util.List;

public class TransactionUtil {

    private TransactionManagerService transactionManagerService;
    private String sourceFileLocation, sFormat, targetFileLocation, tFormat,rFormat;

    private List<Transaction> matched,mismatched,missing;

    public TransactionUtil(String sourceFileLocation, String sFormat, String targetFileLocation, String tFormat, String rFormat) {
        transactionManagerService = new TransactionManager();
        this.sourceFileLocation = sourceFileLocation;
        this.sFormat = sFormat;
        this.targetFileLocation = targetFileLocation;
        this.tFormat = tFormat;
        this.rFormat = rFormat;
        matched = new ArrayList<>();
        mismatched = new ArrayList<>();
        missing = new ArrayList<>();
    }

    public void getResults() {
        List<Transaction> sourceTransactions = parseFile(sourceFileLocation,sFormat);
        List<Transaction> targetTransactions = parseFile(targetFileLocation,tFormat);
        if(sourceTransactions == null || targetTransactions==null) {
            System.out.println("Failed to read files");
            return;
        }
        matched = transactionManagerService.findMatching(sourceTransactions,targetTransactions);
        mismatched = transactionManagerService.findMismatching(sourceTransactions,targetTransactions);
        missing= transactionManagerService.findMissing(sourceTransactions,targetTransactions);
    }

    private List<Transaction> parseFile(String sourceFileLocation, String fileFormat) {
        ParserService parserService;
        if (fileFormat.equalsIgnoreCase("CSV"))
            parserService = new CSVTransactionParser();
        else if (fileFormat.equalsIgnoreCase("JSON"))
            parserService = new JSONTransactionParser();
        else return null;
        return parserService.read(sourceFileLocation);
    }

    public void writeFile(List<Transaction> transactions, String resultFileLocation ) {
        ParserService parserService;
        if (rFormat.equalsIgnoreCase("CSV"))
            parserService = new CSVTransactionParser();
        else if (rFormat.equalsIgnoreCase("JSON"))
            parserService = new JSONTransactionParser();
        else {
            System.out.println("Invalid file format");return;
        }
        parserService.write(transactions, resultFileLocation);
    }

    public List<Transaction> getMatched() {
        return matched;
    }

    public List<Transaction> getMismatched() {
        return mismatched;
    }


    public List<Transaction> getMissing() {
        return missing;
    }

    public TransactionManagerService getTransactionManagerService() {
        return transactionManagerService;
    }

    public String getSourceFileLocation() {
        return sourceFileLocation;
    }

    public String getrFormat() {
        return rFormat;
    }
}
