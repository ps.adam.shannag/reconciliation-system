package com.ps.intern.spring.reconciliation.system.models.auth;

public record AuthenticationResponse(String jwt) {
}
