package com.ps.intern.spring.reconciliation.system.services.Session;

import com.ps.intern.spring.reconciliation.system.models.session.Session;

public interface SessionService {

     boolean save(Session session, String user_name);

}
