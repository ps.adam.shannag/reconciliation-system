package com.ps.intern.spring.reconciliation.system.rest;

import com.ps.intern.spring.reconciliation.system.models.ErrorResponse;
import com.ps.intern.spring.reconciliation.system.models.transaction.TransactionsResponse;
import com.ps.intern.spring.reconciliation.system.util.FileDownloadUtil;
import com.ps.intern.spring.reconciliation.system.util.FileUploadUtil;
import com.ps.intern.spring.reconciliation.system.util.TransactionUtil;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class RestFileController {
    private TransactionUtil transactionUtil;

    @PostMapping(value = "/api/upload")
    public ResponseEntity<?> fileUpload(@RequestParam("fileOne") MultipartFile fileOne,
                                        @RequestParam("fileTwo") MultipartFile fileTwo,
                                        @RequestParam("sFormat") String sFormat,
                                        @RequestParam("tFormat") String tFormat,
                                        @RequestParam("rFormat") String rFormat) throws IOException {

        String fileOneName = StringUtils.cleanPath(fileOne.getOriginalFilename());
        String fileOneCode = FileUploadUtil.saveFile(fileOneName, fileOne);

        String fileTwoName = StringUtils.cleanPath(fileTwo.getOriginalFilename());
        String fileTwoCode = FileUploadUtil.saveFile(fileTwoName, fileTwo);

        String fileOnePath = "Files-Upload/"+fileOneCode+"-"+fileOneName;
        String fileTwoPath = "Files-Upload/"+fileTwoCode+"-"+fileTwoName;


        transactionUtil = new TransactionUtil(fileOnePath, sFormat, fileTwoPath,
                tFormat, rFormat);

        transactionUtil.getResults();

        String matchedPath = "Results-Download/"+fileOneCode+fileTwoCode+"-matched."+rFormat;
        String mismatchedPath = "Results-Download/"+fileOneCode+fileTwoCode+"-mismatched."+rFormat;
        String missingPath = "Results-Download/"+fileOneCode+fileTwoCode+"-missing."+rFormat;

        transactionUtil.writeFile(transactionUtil.getMatched(),matchedPath);
        transactionUtil.writeFile(transactionUtil.getMismatched(),mismatchedPath);
        transactionUtil.writeFile(transactionUtil.getMissing(),missingPath);

        return ResponseEntity.ok(new TransactionsResponse(transactionUtil.getMatched(),
                transactionUtil.getMismatched(), transactionUtil.getMissing(),fileOneCode+fileTwoCode));
    }

    @GetMapping("/api/download/{fileCode}/{fileName}")
    public ResponseEntity<?> downloadFile(@PathVariable("fileCode") String fileCode,@PathVariable("fileName") String fileName) {
        FileDownloadUtil downloadUtil = new FileDownloadUtil();
        Resource resource = null;
        try {
            resource = downloadUtil.getFileAsResource(fileCode+"-"+fileName);
        } catch (IOException e) {
            return ResponseEntity.internalServerError().build();
        }

        if (resource == null) {
            return new ResponseEntity<>("File not found", HttpStatus.NOT_FOUND);
        }

        String contentType = "application/octet-stream";
        String headerValue = "attachment; filename=\"" + resource.getFilename() + "\"";

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, headerValue)
                .body(resource);
    }

    @ExceptionHandler({IOException.class})
    public ResponseEntity<?> handleIOException(){
        return ResponseEntity.status(500).body(new ErrorResponse("File not supported",500));
    }

}
