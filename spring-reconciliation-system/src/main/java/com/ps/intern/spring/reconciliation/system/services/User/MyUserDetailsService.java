package com.ps.intern.spring.reconciliation.system.services.User;

import com.ps.intern.spring.reconciliation.system.dao.UserRepository;
import com.ps.intern.spring.reconciliation.system.models.user.MyUserDetails;
import com.ps.intern.spring.reconciliation.system.models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public MyUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(username);
        user.orElseThrow(()->new UsernameNotFoundException("Not found: "+ username));
        return user.map(MyUserDetails::new).get();
    }
}
