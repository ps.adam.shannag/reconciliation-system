package com.ps.intern.spring.reconciliation.system.models.session;

import com.ps.intern.spring.reconciliation.system.models.activity.Activity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="`session`")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="`id`")
    private int id;

    @Column(name="`session _time`")
    private Date sessionTime;

    @Column(name="`session_expiry`")
    private Date sessionExpiry;

    @OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
    @JoinColumn(name="`session_id`")
    private List<Activity> activities;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(Date sessionTime) {
        this.sessionTime = sessionTime;
    }

    public Date getSessionExpiry() {
        return sessionExpiry;
    }

    public void setSessionExpiry(Date sessionExpiry) {
        this.sessionExpiry = sessionExpiry;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    public void addActivity(Activity activity){
        if(this.activities==null) this.activities = new ArrayList<>();
        this.activities.add(activity);
    }
}
