package com.ps.intern.spring.reconciliation.system;

import com.ps.intern.spring.reconciliation.system.dao.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class SpringReconciliationSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringReconciliationSystemApplication.class, args);
    }


}
