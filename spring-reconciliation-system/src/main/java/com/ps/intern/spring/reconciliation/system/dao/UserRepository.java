package com.ps.intern.spring.reconciliation.system.dao;

import com.ps.intern.spring.reconciliation.system.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUserName(String userName);
}
