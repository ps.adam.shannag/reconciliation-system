package com.ps.intern.spring.reconciliation.system.services.Activity;

import com.ps.intern.spring.reconciliation.system.dao.ActivityRepository;
import com.ps.intern.spring.reconciliation.system.models.activity.Activity;
import com.ps.intern.spring.reconciliation.system.models.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ActivityServiceImpl implements  ActivityService{

    private ActivityRepository activityRepository;
    @Autowired
    public ActivityServiceImpl(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    @Override
    public void save(Activity activity, Session session) {
        session.addActivity(activity);
        activityRepository.save(activity);
    }
}
