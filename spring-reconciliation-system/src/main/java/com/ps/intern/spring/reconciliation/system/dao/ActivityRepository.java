package com.ps.intern.spring.reconciliation.system.dao;

import com.ps.intern.spring.reconciliation.system.models.activity.Activity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActivityRepository extends JpaRepository<Activity, Integer> {
}
