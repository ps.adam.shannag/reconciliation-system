package com.ps.intern.spring.reconciliation.system.rest;

import com.ps.intern.spring.reconciliation.system.models.activity.Activity;
import com.ps.intern.spring.reconciliation.system.models.auth.AuthenticationResponse;
import com.ps.intern.spring.reconciliation.system.models.session.Session;
import com.ps.intern.spring.reconciliation.system.models.session.SessionRequest;
import com.ps.intern.spring.reconciliation.system.models.user.User;
import com.ps.intern.spring.reconciliation.system.services.Activity.ActivityService;
import com.ps.intern.spring.reconciliation.system.services.Session.SessionService;
import com.ps.intern.spring.reconciliation.system.services.User.UserService;
import com.ps.intern.spring.reconciliation.system.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
public class RestDBController {

    private final SessionService sessionService;

    private final ActivityService activityService;

    private final UserService userService;

    private final JwtUtil jwtUtil;


    @Autowired
    public RestDBController(SessionService sessionService, ActivityService activityService, UserService userService, JwtUtil jwtUtil) {
        this.sessionService = sessionService;
        this.activityService = activityService;
        this.userService = userService;
        this.jwtUtil = jwtUtil;
    }

    @PostMapping("/api/save")
    public ResponseEntity<?> save(@RequestBody SessionRequest sessionReq, @RequestAttribute("username") String username, @RequestAttribute("expiry") Date expiry) {
        Activity activity = new Activity();
        activity.setId(0);
        activity.setSource(sessionReq.getSfn());
        activity.setTarget(sessionReq.getTfn());
        activity.setMatched(sessionReq.getMatched());
        activity.setMismatched(sessionReq.getMismatched());
        activity.setMissing(sessionReq.getMissing());
        activity.setTime(new Date());

        User user = userService.findByUserName(username);
        Session currentSession;
        // Get latest session
        if(user.getSessions().isEmpty())
            currentSession =null;
        else currentSession = user.getSessions().get(user.getSessions().size() - 1);

        if (currentSession == null || new Date().after(currentSession.getSessionExpiry())) {
            // Expired session or no session found, create new one
            currentSession = new Session();
            currentSession.setId(0);
            currentSession.setSessionTime(new Date());
            currentSession.setSessionExpiry(expiry);
        }
//        currentSession.setSessionTime(new Date());
        // TODO, this might cause issues as all activities are saved as ID 0 inside the currentSession, (since the ID is not generated randomly yet)
        currentSession.addActivity(activity);
        activityService.save(activity, currentSession);
        sessionService.save(currentSession, username);

        // return a new refresh token
        String refreshToken = jwtUtil.generateToken(username);
        return ResponseEntity.ok(new AuthenticationResponse(refreshToken));
    }

    @GetMapping("/api/load")
    public ResponseEntity<?> load(@RequestAttribute("username") String username){
        User user = userService.findByUserName(username);
        List<Session> sessions;
        // Get latest session
        if(user.getSessions().isEmpty())
            sessions =null;
        else sessions = user.getSessions();
        return ResponseEntity.ok(sessions);
    }


}
