package com.ps.intern.spring.reconciliation.system;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ps.intern.spring.reconciliation.system.dao.UserRepository;
import com.ps.intern.spring.reconciliation.system.models.session.SessionRequest;
import com.ps.intern.spring.reconciliation.system.models.user.MyUserDetails;
import com.ps.intern.spring.reconciliation.system.models.user.User;
import com.ps.intern.spring.reconciliation.system.services.User.UserServiceImpl;
import com.ps.intern.spring.reconciliation.system.util.FileDownloadUtil;
import com.ps.intern.spring.reconciliation.system.util.JwtUtil;
import com.ps.intern.spring.reconciliation.system.util.TransactionUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@SpringBootTest
class SpringReconciliationSystemApplicationTests {
    private TransactionUtil transactionUtil;
    private FileDownloadUtil fileDownloadUtil;
    private MyUserDetails userDetails;
    @Autowired
    private JwtUtil jwtUtil;

    HttpHeaders httpHeaders;

    @Mock
    UserRepository userRepository;
    @InjectMocks
    UserServiceImpl userService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    public static final MediaType APPLICATION_JSON_UTF8 = MediaType.APPLICATION_JSON;


    @BeforeEach
    public void init(){
        transactionUtil = new TransactionUtil("D:\\ps\\spring-reconciliation-system\\Files-Upload\\bank-transactions.csv","csv",
                "D:\\ps\\spring-reconciliation-system\\Files-Upload\\online-banking-transactions.json","json","csv");
        transactionUtil.getResults();
        fileDownloadUtil = new FileDownloadUtil();

        User test_user = new User();
        test_user.setUserName("test_user");
        test_user.setPassword("pass");
        userDetails = new MyUserDetails(test_user);
        jwtUtil = new JwtUtil();
        jwtUtil.setSecretKey("mysecretkey");
        jwtUtil.setExpiry(10);

        // Header for jwt
        httpHeaders = new HttpHeaders();
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("Authorization", "Bearer "+jwtUtil.generateToken(userDetails));
        httpHeaders.setAll(headerMap);
    }


    @Test
    public void whenGetResult_shouldSet_Matched_Mismatched_Missing(){
        assertFalse(transactionUtil.getMatched().isEmpty(),"Matching transactions should be set");
        assertFalse(transactionUtil.getMismatched().isEmpty(),"Mismatching transactions should be set\"");
        assertFalse(transactionUtil.getMissing().isEmpty(),"Missing transactions should be set\"");
    }


    @Test
    public void whenWriteFile_ShouldWrite_FileOfTransactions(){
        transactionUtil.writeFile(transactionUtil.getMatched(),"D:\\ps\\spring-reconciliation-system\\Results-Download\\test-file.csv");
    }

    @Test
    public void whenGetFileAsResource_ReturnFileResource() throws IOException {
        assertNotNull(fileDownloadUtil.getFileAsResource("test"),"Should return a resource of test-file");
    }

    @Test
    public void assertEquals_TestGetUserByUsername_WhenAndVerify(){
        // set up
        when(userRepository.findByUserName("test_user")).thenReturn(Optional.of(new User()));
        // execute
        assertInstanceOf(User.class,userService.findByUserName("test_user"),"Should Get an instance of user");
        // verify
        verify(userRepository).findByUserName("test_user");
    }

    @Test
    public void whenAuthenticate_ReturnJWT() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/authenticate")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(userDetails)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("@.jwt").exists());
    }

    @Test
    public void whenFileUploaded_thenVerifyStatus() throws Exception {
        // Files to upload
        File file = new File("src/test/resources/bank-transactions.csv");
        FileInputStream input = new FileInputStream(file);
        MockMultipartFile fileOne = new MockMultipartFile("fileOne",
                file.getName(), "text/plain", input.readAllBytes());

        File file2 = new File("src/test/resources/online-banking-transactions.json");
        FileInputStream input2 = new FileInputStream(file2);
        MockMultipartFile fileTwo = new MockMultipartFile("fileTwo",
                file2.getName(), "text/plain", input2.readAllBytes());

        // required params
        Map<String, String> params = new HashMap<>();
        params.put("sFormat","csv");
        params.put("tFormat","json");
        params.put("rFormat","csv");
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
        for (Map.Entry<String, String> entry : params.entrySet())
            multiValueMap.put(entry.getKey(), Collections.singletonList(entry.getValue()));

        mockMvc.perform(MockMvcRequestBuilders.multipart("/api/upload")
                .file(fileOne).file(fileTwo).params(multiValueMap).headers(httpHeaders)).andExpect(status().isOk());
    }

    @Test
    public void whenFileDownload_thenVerifyStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/download/test/file.csv").headers(httpHeaders)).andExpect(status().isOk());
    }

    @Test
    public void whenSave_thenVerifyStatus() throws Exception {
        SessionRequest request1 = new SessionRequest("test_file_1","test_file_2",1,2,3);
        mockMvc.perform(MockMvcRequestBuilders.post("/api/save")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request1)).headers(httpHeaders)).andExpect(status().isOk());
    }

    @Test
    public void whenLoad_thenVerifyStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/load").headers(httpHeaders)).andExpect(status().isOk());

    }

}
