package system.comparator;

import com.intern.reconciliation.system.comparator.Comparator;
import com.intern.reconciliation.system.comparator.ComparatorService;
import com.intern.reconciliation.system.transaction.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class ComparatorTest {
    ComparatorService<String> comparatorService;

    @BeforeEach
    public void init(){
        comparatorService = new Comparator<>();
    }

    @Test
    public void shouldReturnAnInstanceOfComparator(){
        assertNotNull(comparatorService, "Should return an instance of a Comparator");
    }

    @Test
    public void shouldReturnTrueIfObjectsAreEqual(){
        String obj1 = "TEST";
        String obj2 ="TEST";
        assertTrue(comparatorService.compare(obj1,obj2),"Should return true, since objects are equal");
    }

    @Test
    public void shouldReturnFalseIfObjectsAreNotEqual(){
        String obj1 = "TEST";
        String obj2 ="TEST2";
        assertFalse(comparatorService.compare(obj1,obj2),"Should return false, since objects are not equal");
    }

    @Test
    public void shouldReturnTrueIfTransactionsAreEqual(){
        Transaction transaction1 = new Transaction();
        Transaction transaction2 = new Transaction();
        Date date = new Date();
        transaction1.setAmount(new BigDecimal("1.00"));
        transaction1.setCurrency("JRD");
        transaction1.setValueDate(date);
        transaction2.setAmount(new BigDecimal("1.00"));
        transaction2.setCurrency("JRD");
        transaction2.setValueDate(date);
        assertTrue(comparatorService.compareTransactions(transaction1,transaction2),"Should return true is transactions are equal");
    }

}
