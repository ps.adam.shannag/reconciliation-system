package system.dao;

import com.intern.reconciliation.system.dao.FileDAO;
import com.intern.reconciliation.system.dao.FileDAOImpl;
import static org.junit.jupiter.api.Assertions.*;

import com.intern.reconciliation.system.exception.FileNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;

public class FileDAOImplTest {
    FileDAO fileDAO;

    @BeforeEach
    public void init(){
        fileDAO = new FileDAOImpl();
    }

    @Test
    public void shouldReturnAnInstanceOfFileDAO(){
        assertNotNull(fileDAO,"Should return an instance of FileDAO");
    }

    @Test
    public void shouldReturnAFileReaderObjectWhenValidSourceFileLocationIsPassed(){
        String sourceFileLocation ="D:\\ps\\Reconciliation system\\sample-files\\input-files\\bank-transactions.csv";
        assertNotNull(fileDAO.read(sourceFileLocation));
    }

    @Test
    public void shouldThrowAFileNotFoundExceptionWhenInvalidSourceFileLocationIsPassed(){
        String sourceFileLocation="...";
        assertThrows(FileNotFoundException.class,()->fileDAO.read(sourceFileLocation),"Should throw a FileNotFoundException");
    }

    @Test
    public void shouldNotThrowAFailedToWriteFileExceptionWhenWritingAFileSuccessfully(){
        String sourceFileLocation="D:\\ps\\Reconciliation system\\sample-files\\test-files\\test-file.txt";
        String testData = "test,test,test";
        StringBuilder builder = new StringBuilder("test,test,test");
        fileDAO.write(builder,sourceFileLocation);
        // check if the file written is correct
        FileReader reader = fileDAO.read(sourceFileLocation);
        int i;
        String content="";
        while(true){
            try {
                if (!((i=reader.read())!=-1)) break;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            content += (char)i;
        }
        assertEquals(testData,content,"Test data and written data should match");
    }

}
