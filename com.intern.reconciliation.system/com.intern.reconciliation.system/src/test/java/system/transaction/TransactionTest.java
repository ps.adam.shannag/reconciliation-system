package system.transaction;


import com.intern.reconciliation.system.comparator.Comparator;
import com.intern.reconciliation.system.parser.CSVTransactionParser;
import com.intern.reconciliation.system.parser.JSONTransactionParser;
import com.intern.reconciliation.system.parser.ParserService;
import com.intern.reconciliation.system.transaction.Transaction;
import com.intern.reconciliation.system.transaction.TransactionManager;
import com.intern.reconciliation.system.transaction.TransactionManagerService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionTest {
    TransactionManagerService transactionManagerService;

    @BeforeEach
    public void init(){
        transactionManagerService = new TransactionManager();
    }

    @Test
    public void shouldReturnAnInstanceOfTransactionManagerService(){
        assertNotNull(transactionManagerService,"Should return an instance of Transaction Manager");
    }

    @Test
    public void shouldReturnAnInstanceOfTransactionManagerServiceWithAComparatorInjected(){
        TransactionManagerService transactionManagerWithComparator = new TransactionManager(new Comparator<>());
        assertNotNull(transactionManagerWithComparator,"Should return an instance of Transaction Manager with a comparator");
    }

    //  Optional, better names for functions, example: givenId_whenSet_thenGet_shouldReturn1(){
    @Test
    public void shouldReturnIdWhenIdIsSet(){
        Transaction transaction = new Transaction();
        String expectedId = "1";
        transaction.setId("1");
        assertEquals(expectedId,transaction.getId(),"Should Return 1 when Id is set to 1");
    }


    @Test
    public void shouldReturnAListOfAllMatchedTransactions(){
        ParserService csvParser = new CSVTransactionParser();
        ParserService jsonParser = new JSONTransactionParser();
        String csvFileName ="D:\\ps\\Reconciliation system\\sample-files\\input-files\\bank-transactions.csv";
        String jsonFileName ="D:\\ps\\Reconciliation system\\sample-files\\input-files\\online-banking-transactions.json";
        assertFalse(transactionManagerService.findMatching(csvParser.read(csvFileName), jsonParser.read(jsonFileName)).isEmpty(), "Should return a list of matched transactions");
    }

    @Test
    public void shouldReturnAListOfAllMismatchedTransactions(){
        ParserService csvParser = new CSVTransactionParser();
        ParserService jsonParser = new JSONTransactionParser();
        String csvFileName ="D:\\ps\\Reconciliation system\\sample-files\\input-files\\bank-transactions.csv";
        String jsonFileName ="D:\\ps\\Reconciliation system\\sample-files\\input-files\\online-banking-transactions.json";
        assertFalse(transactionManagerService.findMismatching(csvParser.read(csvFileName), jsonParser.read(jsonFileName)).isEmpty(), "Should return a list of mismatched transactions");
    }

    @Test
    public void shouldReturnAListOfAllMissingTransactions(){
        ParserService csvParser = new CSVTransactionParser();
        ParserService jsonParser = new JSONTransactionParser();
        String csvFileName ="D:\\ps\\Reconciliation system\\sample-files\\input-files\\bank-transactions.csv";
        String jsonFileName ="D:\\ps\\Reconciliation system\\sample-files\\input-files\\online-banking-transactions.json";
        assertFalse(transactionManagerService.findMissing(csvParser.read(csvFileName), jsonParser.read(jsonFileName)).isEmpty(), "Should return a list of missing transactions");
    }
}
