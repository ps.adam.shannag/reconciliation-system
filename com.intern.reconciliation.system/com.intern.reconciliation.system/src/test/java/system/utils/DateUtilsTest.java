package system.utils;

import com.intern.reconciliation.system.exception.InvalidDateException;
import com.intern.reconciliation.system.utils.DateUtils;
import org.junit.jupiter.api.Test;


import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;


public class DateUtilsTest {

    @Test
    public void shouldReturnADateObjectIfDateIsInValidPattern(){
        assertNotNull(DateUtils.parseDate("2020-01-31","yyyy-MM-dd"),"Should return a date object");
    }

    @Test
    public void shouldThrowAnInvalidDateExceptionIfDateIsInInvalidPatter(){
        assertThrows(InvalidDateException.class,()->DateUtils.parseDate("2020/01/31","yyyy-MM-dd"),"Should throw an InvalidDateException");
    }

    @Test
    public void shouldReturnAFormattedDateStringWithAPattern(){
        assertNotNull(DateUtils.writeDate(new Date(),"yyyy-MM-dd"),"Should return a date object");
    }
}
