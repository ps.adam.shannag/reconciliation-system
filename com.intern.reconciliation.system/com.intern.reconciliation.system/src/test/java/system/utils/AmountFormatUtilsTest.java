package system.utils;


import static org.junit.jupiter.api.Assertions.*;

import com.intern.reconciliation.system.exception.FileNotFoundException;
import com.intern.reconciliation.system.exception.InvalidAmountException;
import com.intern.reconciliation.system.exception.UnknownCurrencyException;
import com.intern.reconciliation.system.utils.AmountFormatUtils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class AmountFormatUtilsTest {
    AmountFormatUtils amountFormatUtils;

    @BeforeEach
    public void init(){
        amountFormatUtils = new AmountFormatUtils();
    }

    @Test
    public void shouldReturnAnInstanceOfAmountFormatUtilsWithACustomPropertiesFileLocation(){
        AmountFormatUtils amountFormatUtils1 = new AmountFormatUtils("currencies.properties");
        assertNotNull(amountFormatUtils1,"Should return an instance of AmountFormatUtils with a custom properties file");
    }

    @Test
    public void shouldThrowInvalidAmountExceptionWhenBigDecimalIsNull(){
        assertThrows(InvalidAmountException.class,()->amountFormatUtils.getAmountFormatPerCurrency(null,"JOD"),"Should throw InvalidAmountException");
    }

   @Test
    public void shouldFormatTheAmountToThreeDecimalsWhenCurrencyIsJOD(){
        int validScale = 3;
        int actualScale = new BigDecimal(amountFormatUtils.getAmountFormatPerCurrency(new BigDecimal("40.0000"),"JOD")).scale();
        assertEquals(validScale, actualScale,"Amount must have 3 decimal places");
   }

    @Test
    public void shouldFormatTheAmountToTwoDecimalsWhenCurrencyIsUSD(){
        int validScale = 2;
        int actualScale = new BigDecimal(amountFormatUtils.getAmountFormatPerCurrency(new BigDecimal("40.0000"),"USD")).scale();
        assertEquals(validScale, actualScale,"Amount must have 2 decimal places");
    }

    @Test
    public void shouldThrowUnknownCurrencyExceptionWhenUnknownCurrencyIsPassed(){
        assertThrows(UnknownCurrencyException.class,()-> amountFormatUtils.getAmountFormatPerCurrency(new BigDecimal("3.000"),"SAD"),
                "Should Throw UnknownCurrencyException when invalid currency is passed");
    }

    @Test
    public void shouldThrowExceptionWhenFileNotFound(){
        assertThrows(FileNotFoundException.class,()->amountFormatUtils.loadProperties("test"),"Should throw FileNotFoundException");
    }
}
