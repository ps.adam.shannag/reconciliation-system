package system.utils;

import com.intern.reconciliation.system.transaction.Transaction;
import com.intern.reconciliation.system.utils.TransactionUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class TransactionUtilsTest {

    List<Transaction> transactions;

    @BeforeEach
    public void init(){
        transactions = new ArrayList<>();
        transactions.add(new Transaction("1",new BigDecimal("1"),"JOD",new Date()));
    }

    @Test
    public void shouldReturnAHeaderForMatchedCSVTransactions(){
        List<String> header = Arrays.stream(new String[]{ "transaction id", "amount", "currency code", "value date"}).toList();
        assertIterableEquals(header, TransactionUtils.getTransactionsCSVHeader(transactions), "Should return a header for matched csv transactions");
    }

    @Test
    public void shouldReturnAHeaderForNotMatchedCSVTransactions(){
        transactions.get(0).setFoundIn("SOURCE");
        List<String> header = Arrays.stream(new String[]{"found in file", "transaction id", "amount", "currency code", "value date"}).toList();
        assertIterableEquals(header, TransactionUtils.getTransactionsCSVHeader(transactions), "Should return a header for not matched csv transactions");
    }

    @Test
    public void shouldReturnARecordOfATransaction(){
        assertNotNull(TransactionUtils.getTransactionAsCSVRecord(transactions.get(0)),"Should return a record of a transaction");
    }
}
