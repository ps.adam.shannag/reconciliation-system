package system.parser;

import com.intern.reconciliation.system.dao.FileDAO;
import com.intern.reconciliation.system.dao.FileDAOImpl;
import com.intern.reconciliation.system.exception.FileNotFoundException;
import com.intern.reconciliation.system.exception.InvalidDateException;
import com.intern.reconciliation.system.exception.InvalidJSONFileException;
import com.intern.reconciliation.system.parser.JSONTransactionParser;
import com.intern.reconciliation.system.parser.ParserService;

import com.intern.reconciliation.system.transaction.Transaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class JSONTransactionParserTest {
    ParserService parserService;

    @BeforeEach
    public void init(){
        parserService = new JSONTransactionParser();
    }

    @Test
    public void shouldReturnAnInstanceOfParser(){
        assertNotNull(parserService,"Should return an instance of ParserService");
    }

    @Test
    public void shouldReturnAnInstanceOfParserWithADAOInjected(){
        FileDAO fileDAO = new FileDAOImpl();
        ParserService parserService1 = new JSONTransactionParser(fileDAO);
        assertNotNull(parserService1,"Should return an instance of parser service with an injected dao");
    }

    @Test
    public void shouldReturnAListOfParsedJSONTransactions()   {
        assertFalse(parserService.read("D:\\ps\\Reconciliation system\\sample-files\\input-files\\online-banking-transactions.json").isEmpty(), "Should return true");
    }

    @Test
    public void shouldThrowExceptionOnReadWhenFileNotFound(){
        assertThrows(FileNotFoundException.class,()-> parserService.read(""),"Should throw an exception, if file not found");
    }

    @Test
    public void shouldThrowExceptionWhenInvalidJSONFileParsed(){
        assertThrows(InvalidJSONFileException.class,()-> parserService.read("D:\\ps\\Reconciliation system\\sample-files\\input-files\\bank-transactions-bad-date.csv"),
                "Should throw an exception, if invalid json file");
    }

    @Test
    public void shouldThrowExceptionWhenInvalidDateParsed(){
        assertThrows(InvalidDateException.class,()-> parserService.read("D:\\ps\\Reconciliation system\\sample-files\\input-files\\online-banking-transactions-bad-date.json"),
                "Should throw an exception, if invalid date is passed");
    }

    @Test
    public void shouldWriteAJSONFile(){
        List<Transaction> transactions =
                parserService.read("D:\\ps\\Reconciliation system\\sample-files\\input-files\\online-banking-transactions.json");
       parserService.write(transactions,"D:\\ps\\Reconciliation system\\sample-files\\result-files\\bank-transactions-test.json");
    }


}
