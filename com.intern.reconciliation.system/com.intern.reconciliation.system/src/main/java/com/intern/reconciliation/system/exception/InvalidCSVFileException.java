package com.intern.reconciliation.system.exception;

public class InvalidCSVFileException extends RuntimeException{
    public InvalidCSVFileException(String message) {
        super(message);
    }
}
