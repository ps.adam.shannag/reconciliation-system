package com.intern.reconciliation.system.exception;

public class FailedToWriteFileException extends RuntimeException{
    public FailedToWriteFileException(String message) {
        super(message);
    }

}
