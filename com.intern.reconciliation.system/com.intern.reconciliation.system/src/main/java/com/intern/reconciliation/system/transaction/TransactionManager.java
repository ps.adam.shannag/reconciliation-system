package com.intern.reconciliation.system.transaction;

import com.intern.reconciliation.system.comparator.Comparator;
import com.intern.reconciliation.system.comparator.ComparatorService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TransactionManager implements TransactionManagerService {
    private final ComparatorService<String> comparator;

    public TransactionManager() {
        this.comparator = new Comparator<>();
    }

    public TransactionManager(ComparatorService<String> comparator) {
        this.comparator = comparator;
    }

    @Override
    public List<Transaction> findMatching(List<Transaction> firstTransactions, List<Transaction> secondTransactions) {
        List<Transaction> matchingTransactions = new ArrayList<>();
        for (Transaction firstTransaction : firstTransactions) {
            for (Transaction secondTransaction : secondTransactions) {
                if (comparator.compare(firstTransaction.getId(), secondTransaction.getId())) {
                    if (comparator.compareTransactions(firstTransaction, secondTransaction)) {
                        matchingTransactions.add(firstTransaction);
                    }
                    break;
                }
            }
        }
        return matchingTransactions;
    }

    @Override
    public List<Transaction> findMismatching(List<Transaction> firstTransactions, List<Transaction> secondTransactions) {
        List<Transaction> mismatchingTransactions = new ArrayList<>();
        for (Transaction firstTransaction : firstTransactions) {
            for (Transaction secondTransaction : secondTransactions) {
                if (comparator.compare(firstTransaction.getId(), secondTransaction.getId())) {
                    if (!comparator.compareTransactions(firstTransaction, secondTransaction)) {
                        firstTransaction.setFoundIn("SOURCE");
                        secondTransaction.setFoundIn("TARGET");
                        mismatchingTransactions.add(firstTransaction);
                        mismatchingTransactions.add(secondTransaction);
                    }
                    break;
                }
            }
        }
        return mismatchingTransactions;
    }

    @Override
    public List<Transaction> findMissing(List<Transaction> firstTransactions, List<Transaction> secondTransactions) {
        List<Transaction> missingTransactions = new ArrayList<>();

        for (Iterator<Transaction> iterator = firstTransactions.iterator();iterator.hasNext();){
            Transaction transaction1 =iterator.next();
            for(Iterator<Transaction> iterator2 = secondTransactions.iterator(); iterator2.hasNext();){
                Transaction transaction2 = iterator2.next();
                if(comparator.compare(transaction1.getId(),transaction2.getId())){
                    iterator.remove();
                    iterator2.remove();
                }
            }
        }

        // set source/target values
        for(Transaction transaction : firstTransactions) {
            transaction.setFoundIn("SOURCE");
            missingTransactions.add(transaction);
        }
        for(Transaction transaction : secondTransactions){
            transaction.setFoundIn("TARGET");
            missingTransactions.add(transaction);
        }

        return missingTransactions;
    }
}
