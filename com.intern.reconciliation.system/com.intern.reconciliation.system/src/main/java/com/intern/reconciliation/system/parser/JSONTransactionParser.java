package com.intern.reconciliation.system.parser;

import com.intern.reconciliation.system.dao.FileDAO;
import com.intern.reconciliation.system.dao.FileDAOImpl;
import com.intern.reconciliation.system.exception.FileNotFoundException;
import com.intern.reconciliation.system.exception.InvalidDateException;
import com.intern.reconciliation.system.exception.InvalidJSONFileException;
import com.intern.reconciliation.system.transaction.Transaction;
import com.intern.reconciliation.system.utils.AmountFormatUtils;
import com.intern.reconciliation.system.utils.DateUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

public class JSONTransactionParser implements ParserService {

    JSONParser parser;
    FileDAO fileDAO;

    public JSONTransactionParser() {
        parser = new org.json.simple.parser.JSONParser();
        fileDAO = new FileDAOImpl();
    }

    public JSONTransactionParser(FileDAO fileDAO) {
        parser = new org.json.simple.parser.JSONParser();
        this.fileDAO = fileDAO;
    }

    @Override
    public List<Transaction> read(String sourceFileLocation) {
        List<Transaction> transactions = new ArrayList<>();
        try {
            Object obj = parser.parse(fileDAO.read(sourceFileLocation));
            JSONArray transactionList = (JSONArray) obj;
            transactionList.forEach(transaction -> {
                try {
                   transactions.add(parseTransactionObject((JSONObject) transaction));
                } catch (java.text.ParseException e) {
                    throw new InvalidDateException("Invalid date format parsed");
                }
            });
        } catch (IOException e) {
            throw new FileNotFoundException("File not found");
        } catch (ParseException e) {
            throw new InvalidJSONFileException("Error reading JSON file");
        }
        return transactions;
    }


    public List<Transaction> read(File file) {
        List<Transaction> transactions = new ArrayList<>();
        try {
            Object obj = parser.parse(String.valueOf(file));
            JSONArray transactionList = (JSONArray) obj;
            transactionList.forEach(transaction -> {
                try {
                    transactions.add(parseTransactionObject((JSONObject) transaction));
                } catch (java.text.ParseException e) {
                    throw new InvalidDateException("Invalid date format parsed");
                }
            });
        } catch (ParseException e) {
            throw new InvalidJSONFileException("Error reading JSON file");
        }
        return transactions;
    }

    @Override
    public void write(List<Transaction> transactions, String sourceFileLocation) {
        Map<String,String> transactionsMap = new HashMap<>();
        JSONArray transactionList = new JSONArray();
        for(Transaction transaction:transactions){
            if(transaction.getFoundIn() !="-/-") transactionsMap.put("found in",transaction.getFoundIn());
            transactionsMap.put("date",DateUtils.writeDate(transaction.getValueDate(),"dd/MM/yyyy"));
            transactionsMap.put("reference",transaction.getId());
            transactionsMap.put("amount", new AmountFormatUtils().getAmountFormatPerCurrency(transaction.getAmount(),transaction.getCurrency()));
            transactionsMap.put("currencyCode",transaction.getCurrency());
            JSONObject JSONTransaction = new JSONObject();
            JSONTransaction.putAll(transactionsMap);
            transactionList.add(JSONTransaction);
        }
        StringBuilder builder = new StringBuilder(transactionList.toJSONString());
        fileDAO.write(builder,sourceFileLocation);
    }

    private Transaction parseTransactionObject(JSONObject transaction) throws java.text.ParseException {
        try{
            return new Transaction(
                    (String) transaction.get("reference"),
                    new BigDecimal((String) transaction.get("amount")),
                    (String) transaction.get("currencyCode"),
                    DateUtils.parseDate((String) transaction.get("date"),"dd/MM/yyyy")
            );
        }catch(NumberFormatException e){
            throw new InvalidJSONFileException("Failed to read JSON file, please check the amount field.");
        }
    }
}
