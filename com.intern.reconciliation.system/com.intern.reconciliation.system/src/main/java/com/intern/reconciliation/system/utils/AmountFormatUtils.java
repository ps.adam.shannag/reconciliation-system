package com.intern.reconciliation.system.utils;

import com.intern.reconciliation.system.exception.FileNotFoundException;
import com.intern.reconciliation.system.exception.UnknownCurrencyException;
import com.intern.reconciliation.system.exception.InvalidAmountException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class AmountFormatUtils {
    private Map<String, Integer> currencies;

    public AmountFormatUtils() {
        this.currencies = new HashMap<>();
        loadProperties("currencies.properties");
    }

    public AmountFormatUtils(String currenciesFile){
        this.currencies = new HashMap<>();
        loadProperties(currenciesFile);
    }
    public  String getAmountFormatPerCurrency(BigDecimal amount, String currency){
        try{
            return amount.setScale(decimalPointsFor(currency)).toString();
        }catch (UnknownCurrencyException e){
            throw new UnknownCurrencyException("Unknown currency");
        }
        catch (Exception e){
            throw new InvalidAmountException("Invalid amount entered");
        }
    }

    private  int decimalPointsFor(String currency){
        try{
            return this.currencies.get(currency);
        }catch (NullPointerException e){
            throw new UnknownCurrencyException("Unknown currency");
        }
    }

    public void loadProperties(String fileName){
        Properties currencies = new Properties();
        this.currencies = new HashMap<>();
        try {
            currencies.load(ClassLoader.getSystemClassLoader().getResourceAsStream(fileName));
            currencies.forEach((key,value) -> this.currencies.put(key.toString(),Integer.parseInt(value.toString())));
        } catch (Exception e) {
            throw new FileNotFoundException("Invalid properties file");
        }
    }
}
