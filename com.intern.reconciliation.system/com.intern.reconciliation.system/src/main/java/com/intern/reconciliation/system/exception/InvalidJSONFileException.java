package com.intern.reconciliation.system.exception;

public class InvalidJSONFileException extends RuntimeException{
    public InvalidJSONFileException(String message) {
        super(message);
    }

}
