package com.intern.reconciliation.system.transaction;

import java.math.BigDecimal;
import java.util.Date;

public class Transaction {

    private String id;
    private BigDecimal amount;
    private String currency;
    private Date valueDate;
    private String foundIn;

    public Transaction() {
    }

    public Transaction(String id, BigDecimal amount, String currency, Date valueDate) {
        this.id = id;
        this.amount = amount;
        this.currency = currency;
        this.valueDate = valueDate;
        this.foundIn="-/-";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public String getFoundIn() {
        return foundIn;
    }

    public void setFoundIn(String foundIn) {
        this.foundIn = foundIn;
    }
}
