package com.intern.reconciliation.system.transaction;

import java.util.List;

public interface TransactionManagerService {
    List<Transaction> findMatching(List<Transaction> firstTransactions, List<Transaction> secondTransactions);
    List<Transaction> findMismatching(List<Transaction> firstTransactions, List<Transaction> secondTransactions);
    List<Transaction> findMissing(List<Transaction> firstTransactions, List<Transaction> secondTransactions);
}
