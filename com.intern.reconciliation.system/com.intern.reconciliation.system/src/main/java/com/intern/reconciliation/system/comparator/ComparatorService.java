package com.intern.reconciliation.system.comparator;

import com.intern.reconciliation.system.transaction.Transaction;

public interface ComparatorService<T>  {
    boolean compare(T obj1, T obj2);
    boolean compareTransactions(Transaction transactionOne, Transaction transactionTwo);
}
