package com.intern.reconciliation.system.parser;

import com.intern.reconciliation.system.transaction.Transaction;

import java.util.List;

public interface ParserService {
    List<Transaction> read(String sourceFileLocation);
    void write(List<Transaction> transactions, String sourceFileLocation);
}
