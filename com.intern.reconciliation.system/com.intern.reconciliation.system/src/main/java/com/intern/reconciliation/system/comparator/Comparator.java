package com.intern.reconciliation.system.comparator;

import com.intern.reconciliation.system.transaction.Transaction;

public class Comparator<T> implements ComparatorService<T> {

    @Override
    public boolean compare(T obj1, T obj2) {
        return obj1.equals(obj2);
    }

    @Override
    public boolean compareTransactions(Transaction transactionOne, Transaction transactionTwo) {
        if (transactionOne.getAmount().compareTo(transactionTwo.getAmount()) ==0 &&
                transactionOne.getCurrency().equals(transactionTwo.getCurrency()) &&
                transactionOne.getValueDate().equals(transactionTwo.getValueDate()))
            return true;
        return false;
    }
}
