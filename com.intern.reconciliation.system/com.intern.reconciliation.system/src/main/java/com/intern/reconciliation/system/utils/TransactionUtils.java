package com.intern.reconciliation.system.utils;

import com.intern.reconciliation.system.transaction.Transaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransactionUtils {

    public static List<String> getTransactionsCSVHeader(List<Transaction> transactions){
        List<String> header = Arrays.stream(new String[]{ "transaction id", "amount", "currency code", "value date"}).toList();
        List<String> header2 = Arrays.stream(new String[]{"found in file", "transaction id", "amount", "currency code", "value date"}).toList();
        if(!transactions.get(0).getFoundIn().equals("-/-"))
            return header2;
        return header;
    }

    public static List<String> getTransactionAsCSVRecord(Transaction transaction){
        List<String> record = new ArrayList<>();
        if(!transaction.getFoundIn().equals("-/-"))
            record.add(transaction.getFoundIn());
        record.add(transaction.getId());
        record.add(new AmountFormatUtils().getAmountFormatPerCurrency(transaction.getAmount(),transaction.getCurrency()));
        record.add(transaction.getCurrency());
        record.add(DateUtils.writeDate(transaction.getValueDate(),"yyyy-MM-dd"));
        return record;
    }
}
