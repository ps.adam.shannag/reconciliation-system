package com.intern.reconciliation.system.dao;

import java.io.FileReader;

public interface FileDAO {
    FileReader read(String sourceFileLocation);
    void write(StringBuilder builder,String sourceFileLocation);
}
