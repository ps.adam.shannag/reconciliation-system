package com.intern.reconciliation.system.dao;

import com.intern.reconciliation.system.exception.FailedToWriteFileException;

import java.io.*;

public class FileDAOImpl implements FileDAO{
    @Override
    public FileReader read(String sourceFileLocation) {
        try {
            return new FileReader(sourceFileLocation);
        } catch (FileNotFoundException e) {
           throw new com.intern.reconciliation.system.exception.FileNotFoundException("File not found");
        }
    }

    @Override
    public void write(StringBuilder builder, String sourceFileLocation) {
        FileWriter writer;
        try {
            writer = new FileWriter(sourceFileLocation);
            writer.write(builder.toString());
            writer.flush();
        } catch (IOException e) {
            throw new FailedToWriteFileException("Error writing file");
        }

    }
}
