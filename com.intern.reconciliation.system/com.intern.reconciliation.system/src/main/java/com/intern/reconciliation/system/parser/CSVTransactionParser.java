package com.intern.reconciliation.system.parser;

import com.intern.reconciliation.system.dao.FileDAO;
import com.intern.reconciliation.system.dao.FileDAOImpl;
import com.intern.reconciliation.system.exception.FileNotFoundException;
import com.intern.reconciliation.system.transaction.Transaction;
import com.intern.reconciliation.system.exception.InvalidCSVFileException;
import com.intern.reconciliation.system.utils.DateUtils;
import com.intern.reconciliation.system.utils.TransactionUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CSVTransactionParser implements ParserService {
    CSVParser parser;
    CSVPrinter printer;
    FileDAO fileDAO;

    public CSVTransactionParser() {
        this.fileDAO = new FileDAOImpl();
    }

    public CSVTransactionParser(FileDAO fileDAO) {
        this.fileDAO = fileDAO;
    }

    @Override
    public List<Transaction> read(String sourceFileLocation) {
        List<Transaction> transactions = new ArrayList<>();
        try {
            parser = new CSVParser(fileDAO.read(sourceFileLocation), CSVFormat.DEFAULT.withHeader());
            for (CSVRecord record : parser) {
                Transaction transaction;
                transaction = new Transaction(
                        record.get("trans unique id"),
                        new BigDecimal(record.get("amount")),
                        record.get("currecny"),
                        DateUtils.parseDate(record.get("value date"),"yyyy-MM-dd"));
                transactions.add(transaction);
            }
            parser.close();
        } catch (IOException e) {
            throw new FileNotFoundException("File not found");
        } catch (NumberFormatException e){
            throw new InvalidCSVFileException("Failed to read CSV file, please check the amount field.");
        }
        return transactions;
    }

    @Override
    public void write(List<Transaction> transactions, String sourceFileLocation) {
        CSVFormat format =CSVFormat.RFC4180.withHeader().withDelimiter(',');
        StringBuilder builder = new StringBuilder();
        try {
            printer = new CSVPrinter(builder,format);
            printer.printRecord(TransactionUtils.getTransactionsCSVHeader(transactions));
            for(Transaction transaction:transactions){
              printer.printRecord(TransactionUtils.getTransactionAsCSVRecord(transaction));
            }
            fileDAO.write(builder,sourceFileLocation);

        } catch (IOException e) {
            throw new RuntimeException("Failed to write file");
        }

    }
}
