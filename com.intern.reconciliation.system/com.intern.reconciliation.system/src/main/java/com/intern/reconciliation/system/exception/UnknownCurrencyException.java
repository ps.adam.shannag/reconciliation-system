package com.intern.reconciliation.system.exception;

public class UnknownCurrencyException extends RuntimeException{
    public UnknownCurrencyException(String message) {
        super(message);
    }
}
