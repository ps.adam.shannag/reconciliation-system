package com.intern.reconciliation.app;


import com.intern.reconciliation.system.parser.CSVTransactionParser;
import com.intern.reconciliation.system.parser.JSONTransactionParser;
import com.intern.reconciliation.system.parser.ParserService;
import com.intern.reconciliation.system.transaction.Transaction;
import com.intern.reconciliation.system.transaction.TransactionManager;
import com.intern.reconciliation.system.transaction.TransactionManagerService;

import java.util.List;

public class TransactionReconciliation {

    TransactionManagerService transactionManagerService;
    private String sourceFileLocation, sFormat, targetFileLocation, tFormat,rFormat;


    public TransactionReconciliation(String sourceFileLocation, String sFormat, String targetFileLocation, String tFormat, String rFormat) {
        this.transactionManagerService = new TransactionManager();
        this.sourceFileLocation = sourceFileLocation;
        this.sFormat = sFormat;
        this.targetFileLocation = targetFileLocation;
        this.tFormat = tFormat;
        this.rFormat = rFormat;
    }

    public void getResults() {
        List<Transaction> sourceTransactions = parseFile(sourceFileLocation,sFormat);
        List<Transaction> targetTransactions = parseFile(targetFileLocation,tFormat);
        if(sourceTransactions == null || targetTransactions==null) {
            System.out.println("Failed to read files");
            return;
        }
        writeFile(transactionManagerService.findMatching(sourceTransactions,targetTransactions),
                "D:\\ps\\reconciliation\\result-files\\matchingTransactions."+this.rFormat.toLowerCase(),this.rFormat);
        writeFile(transactionManagerService.findMismatching(sourceTransactions,targetTransactions),
                "D:\\ps\\reconciliation\\result-files\\mismatchingTransactions."+this.rFormat.toLowerCase(),this.rFormat);
        writeFile(transactionManagerService.findMissing(sourceTransactions,targetTransactions),
                "D:\\ps\\reconciliation\\result-files\\missingTransactions."+this.rFormat.toLowerCase(),this.rFormat);
        System.out.println("Reconciliation finished.\n" +
                "Result files are available in directory D:\\ps\\reconciliation\\result-files\\\n");
    }

    private List<Transaction> parseFile(String sourceFileLocation, String fileFormat) {
        ParserService parserService;
        if (fileFormat.equalsIgnoreCase("CSV"))
            parserService = new CSVTransactionParser();
        else if (fileFormat.equalsIgnoreCase("JSON"))
            parserService = new JSONTransactionParser();
        else return null;
        return parserService.read(sourceFileLocation);
    }

    private void writeFile(List<Transaction> transactions, String resultFileLocation ,String fileFormat) {
        ParserService parserService;
        if (fileFormat.equalsIgnoreCase("CSV"))
            parserService = new CSVTransactionParser();
        else if (fileFormat.equalsIgnoreCase("JSON"))
            parserService = new JSONTransactionParser();
        else {
            System.out.println("Invalid file format");return;
        }
        parserService.write(transactions, resultFileLocation);
    }
}
