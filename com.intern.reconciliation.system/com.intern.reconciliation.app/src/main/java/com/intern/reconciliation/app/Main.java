package com.intern.reconciliation.app;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        TransactionReconciliation transactionReconciliation ;
        Scanner scan = new Scanner(System.in);
        String sourceFileLocation,sFormat,targetFileLocation,tFormat,rFormat;

        System.out.println(">> Enter source file location:");
        sourceFileLocation= scan.nextLine();
        System.out.println(">> Enter source file format:");
        sFormat= scan.nextLine();
        System.out.println(">> Enter target file location:");
        targetFileLocation= scan.nextLine();
        System.out.println(">> Enter target file format:");
        tFormat= scan.nextLine();
        System.out.println(">> Enter result files format:");
        rFormat= scan.nextLine();

        transactionReconciliation = new TransactionReconciliation(sourceFileLocation,sFormat,targetFileLocation,tFormat,rFormat);
        transactionReconciliation.getResults();

        scan.close();

    }
}
